package com.cognizant.retailamate.jda.Model;

/**
 * Created by 452781 on 11/22/2016.
 */
public class AddAssociateModel {

    String UserId;
    String BeaconId;
    String CaptureTime;
    Double XPosition;
    Double YPosition;

    public Double getXPosition() {
        return XPosition;
    }

    public void setXPosition(Double XPosition) {
        this.XPosition = XPosition;
    }

    public Double getYPosition() {
        return YPosition;
    }

    public void setYPosition(Double YPosition) {
        this.YPosition = YPosition;
    }

    public String getAssociateId() {
        return UserId;
    }

    public void setAssociateId(String associateId) {
        UserId = associateId;
    }

    public String getBeaconId() {
        return BeaconId;
    }

    public void setBeaconId(String beaconId) {
        BeaconId = beaconId;
    }

    public String getCaptureTime() {
        return CaptureTime;
    }

    public void setCaptureTime(String captureTime) {
        CaptureTime = captureTime;
    }
}
