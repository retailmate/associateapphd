package com.cognizant.retailamate.jda.Model;

/**
 * Created by 452781 on 11/23/2016.
 */
public class PositionModel {

    /**
     * UserId : 1000235
     * StartTime : 2016-11-23T06:00:00
     * EndTime : 2016-11-23T17:00:00
     * SiteId : 1000553
     * JobName : Store Manager
     * BeaconId : b9407f30-f5f8-466e-aef9-25556b57fe6a
     * FirstName : John
     * LastName : Shinde
     * AssociateCount : 1
     * XPosition : 200.0
     * YPosition : 200.0
     */

    private String UserId;
    private String StartTime;
    private String EndTime;
    private int SiteId;
    private String JobName;
    private String BeaconId;
    private String FirstName;
    private String LastName;
    private int AssociateCount;
    Double XPosition;
    Double YPosition;

    public Double getXPosition() {
        return XPosition;
    }

    public void setXPosition(Double XPosition) {
        this.XPosition = XPosition;
    }

    public Double getYPosition() {
        return YPosition;
    }

    public void setYPosition(Double YPosition) {
        this.YPosition = YPosition;
    }

    public String getAssociateId() {
        return UserId;
    }

    public void setAssociateId(String AssociateId) {
        this.UserId = AssociateId;
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String StartTime) {
        this.StartTime = StartTime;
    }

    public String getEndTime() {
        return EndTime;
    }

    public void setEndTime(String EndTime) {
        this.EndTime = EndTime;
    }

    public int getSiteId() {
        return SiteId;
    }

    public void setSiteId(int SiteId) {
        this.SiteId = SiteId;
    }

    public String getJobName() {
        return JobName;
    }

    public void setJobName(String JobName) {
        this.JobName = JobName;
    }

    public String getBeaconId() {
        return BeaconId;
    }

    public void setBeaconId(String BeaconId) {
        this.BeaconId = BeaconId;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    public int getAssociateCount() {
        return AssociateCount;
    }

    public void setAssociateCount(int AssociateCount) {
        this.AssociateCount = AssociateCount;
    }
}
