package com.cognizant.retailamate.jda.manager;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.cognizant.retailamate.jda.R;
import com.cognizant.retailamate.jda.associate.CustomerIndoorLocation;

public class HighValueCustomerInBound extends AppCompatActivity {

    TextView showLocation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.highvalue_customer_inbound);

        showLocation = (TextView) findViewById(R.id.show_location);

        showLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HighValueCustomerInBound.this, CustomerIndoorLocation.class);
                startActivity(intent);


            }
        });
    }


}
