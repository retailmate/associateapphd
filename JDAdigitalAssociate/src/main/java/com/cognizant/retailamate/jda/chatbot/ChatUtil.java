package com.cognizant.retailamate.jda.chatbot;

import android.annotation.TargetApi;
import android.os.Build;
import android.speech.tts.TextToSpeech;
import android.text.format.DateFormat;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by 540472 on 12/27/2016.
 */
public class ChatUtil {

    private static final String CustomTag = "CustomTag";


    public static void setChatReplyMessage(Message reply) {
        ChatDataModel chatSendData = new ChatDataModel();
        chatSendData.setmDataset(reply.getText());
        chatSendData.setmDatasetTypes(ChatGlobal.RECEIVE);
        Date d = reply.getDate();
        String time = (String) DateFormat.format("HH:mm", d.getTime());
        chatSendData.setmTime(time);
        ChatGlobal.chatDataModels.add(chatSendData);
        ChatGlobal.mAdapter.notifyDataSetChanged();
        ChatGlobal.mRecyclerView.scrollToPosition(ChatGlobal.chatDataModels.size() - 1);
    }

    public void setChatSendMessage(Message userSays) {
        ChatDataModel chatSendData = new ChatDataModel();
        chatSendData.setmDataset(userSays.getText());
        chatSendData.setmDatasetTypes(ChatGlobal.SEND);
        Date d = userSays.getDate();
        String time = (String) DateFormat.format("HH:mm", d.getTime());
        chatSendData.setmTime(time);
        ChatGlobal.chatDataModels.add(chatSendData);
        ChatGlobal.mAdapter.notifyDataSetChanged();
        ChatGlobal.mRecyclerView.scrollToPosition(ChatGlobal.chatDataModels.size() - 1);
    }


}
