package com.cognizant.retailamate.jda.buddystore.popup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.cognizant.retailamate.jda.R;
import com.cognizant.retailamate.jda.buddystore.activity.BuddyStoreSelectionScreen;
import com.cognizant.retailamate.jda.buddystore.activity.RMProductScreen;
import com.cognizant.retailamate.jda.buddystore.interfaces.ClearInterface;


public class RequestModePopUp extends AppCompatActivity implements View.OnClickListener, ClearInterface {
    private static final int RESULT_CLOSE_ALL = 0;

    Context mContext;
    Button ok, cancel;
    NumberPicker numberPicker;
    int position;
    TextView type, color;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.request_mode_pop_up);


        type = (TextView) findViewById(R.id.type_tv);
        color = (TextView) findViewById(R.id.color_tv);

        position = getIntent().getIntExtra("position", 0);

        type.setText("Type : " + RMProductScreen.product.getVariants().get(position).getSize());
        color.setText("Color : " + RMProductScreen.product.getVariants().get(position).getColor());


        mContext = getApplicationContext();

        ok = (Button) findViewById(R.id.ok);
        cancel = (Button) findViewById(R.id.cancel);
        numberPicker = (NumberPicker) findViewById(R.id.quantity_Picker);
        numberPicker.setMaxValue(100);
        numberPicker.setMinValue(1);
        numberPicker.setWrapSelectorWheel(false);


        ok.setOnClickListener(this);
        cancel.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.ok:
                Intent intent = new Intent(mContext, BuddyStoreSelectionScreen.class);
                intent.putExtra("quantity", numberPicker.getValue());
                startActivityForResult(intent,RESULT_CLOSE_ALL);

                break;
            case R.id.cancel:
                finish();
                break;

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (resultCode) {
            case RESULT_CLOSE_ALL:
                setResult(RESULT_CLOSE_ALL);

                finish();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void clearActivity() {
        finish();
    }
}
