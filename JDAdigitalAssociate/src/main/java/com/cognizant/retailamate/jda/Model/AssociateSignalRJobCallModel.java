package com.cognizant.retailamate.jda.Model;

/**
 * Created by 452781 on 12/11/2016.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AssociateSignalRJobCallModel {


    @SerializedName("assocID")
    @Expose
    private Object assocID;

    /**
     * @return The assocID
     */
    public Object getAssocID() {
        return assocID;
    }

    /**
     * @param assocID The assocID
     */
    public void setAssocID(Object assocID) {
        this.assocID = assocID;
    }

}


