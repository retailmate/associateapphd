package com.cognizant.retailamate.jda.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.support.multidex.MultiDexApplication;

import com.cognizant.retailamate.jda.Model.PositionModel;
import com.firebase.client.Firebase;

import java.util.HashMap;

/**
 * Created by 452781 on 11/22/2016.
 */
public class GlobalClass extends MultiDexApplication {
    public static String associateUserName;
    public static String associateId;
    public static String associateName;
    public static String associateStartTime;
    public static String associateEndTime;
    public static String associateDesignation;
    public static String appLanguage;

    public static PositionModel[] associatePositionModel;

    public static PositionModel[] customerPositionModel;


    public static HashMap<String, String> associateBeacon = new HashMap<>();

    public static HashMap<String, String> customerBeacon = new HashMap<>();

    public static String getLanguage() {
        if (appLanguage.equals(Constants.english))
            return "ar";
        else if (appLanguage.equals(Constants.arabic))
            return "en";
        else
            return null;
    }


    public static void setAssociateBeacon(HashMap<String, String> associateBeacon) {
        GlobalClass.associateBeacon = associateBeacon;
    }

    public static HashMap<String, String> getAssociateBeacon() {
        return associateBeacon;
    }

//    public static String loginURl="http://services-useast.skytap.com:17199/retail/data/login?loginName=";

    public static final String TAG = GlobalClass.class.getSimpleName();
    private static Context sContext;
    private static GlobalClass mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        GlobalClass.setAppContext(getApplicationContext());
        Firebase.setAndroidContext(this);
        mInstance = this;
        appLanguage = Constants.english;
    }

    public static void setAppContext(Context context) {
        Log.d(TAG, String.format("setAppContext: %s", context));
        sContext = context;
    }

    public static Context getAppContext() {
        return sContext;
    }

    public static synchronized GlobalClass getInstance() {
        return mInstance;
    }

    public static boolean checkPermission(Activity activity, String permission) {
        int result = ContextCompat.checkSelfPermission(activity, permission);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

}
