package com.cognizant.retailamate.jda.associate;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.retailamate.jda.R;

public class ProductDelivery extends AppCompatActivity {

    TextView product_ok;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_delivery_popup);

        product_ok = (TextView) findViewById(R.id.product_deliver_ok);

        product_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProductDelivery.this,CustomerInBound.class);
                startActivity(intent);
            }
        });
    }
}
