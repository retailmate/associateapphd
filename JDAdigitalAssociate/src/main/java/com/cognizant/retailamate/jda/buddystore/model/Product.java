package com.cognizant.retailamate.jda.buddystore.model;

import java.util.List;

/**
 * Created by Bharath on 23/03/17.
 */

public class Product {


    /**
     * Store : Max Store - Emirates Mall S1
     * Name : Pepe Men Navy
     * ID : #1246443
     * Category : Men Dept.
     * Variants : [{"Size":"38","Color":"Black","Price":"49.05","SOH":{"Shelf":"5","BackRoom":"4"}},{"Size":"40","Color":"Blue","Price":"49.05","SOH":{"Shelf":"0","BackRoom":"0"}},{"Size":"42","Color":"Blue","Price":"49.05","SOH":{"Shelf":"1","BackRoom":"2"}}]
     */

    private String Store;
    private String Name;
    private String ID;
    private String Category;
    private List<VariantsBean> Variants;

    public String getStore() {
        return Store;
    }

    public void setStore(String Store) {
        this.Store = Store;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String Category) {
        this.Category = Category;
    }

    public List<VariantsBean> getVariants() {
        return Variants;
    }

    public void setVariants(List<VariantsBean> Variants) {
        this.Variants = Variants;
    }

    public static class VariantsBean {
        /**
         * Size : 38
         * Color : Black
         * Price : 49.05
         * SOH : {"Shelf":"5","BackRoom":"4"}
         */

        private String Size;
        private String Color;
        private String Price;
        private SOHBean SOH;

        public String getSize() {
            return Size;
        }

        public void setSize(String Size) {
            this.Size = Size;
        }

        public String getColor() {
            return Color;
        }

        public void setColor(String Color) {
            this.Color = Color;
        }

        public String getPrice() {
            return Price;
        }

        public void setPrice(String Price) {
            this.Price = Price;
        }

        public SOHBean getSOH() {
            return SOH;
        }

        public void setSOH(SOHBean SOH) {
            this.SOH = SOH;
        }

        public static class SOHBean {
            /**
             * Shelf : 5
             * BackRoom : 4
             */

            private String Shelf;
            private String BackRoom;

            public String getShelf() {
                return Shelf;
            }

            public void setShelf(String Shelf) {
                this.Shelf = Shelf;
            }

            public String getBackRoom() {
                return BackRoom;
            }

            public void setBackRoom(String BackRoom) {
                this.BackRoom = BackRoom;
            }
        }
    }
}
