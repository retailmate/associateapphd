package com.cognizant.retailamate.jda.unused;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;

import com.cognizant.retailamate.jda.R;

/**
 * Created by 452781 on 11/28/2016.
 */
public class AssociateTasksActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.associate_tasks);
        Toolbar mToolBar = (Toolbar) findViewById(R.id.toolbar);
        mToolBar.setNavigationIcon(R.drawable.back);
        mToolBar.setTitle(R.string.app_name);
        mToolBar.setTitleTextColor(Color.WHITE);
        mToolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.associate_task_layout);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AssociateTasksActivity.this, com.cognizant.retailamate.jda.associatetasks.AssociateJobListActivity.class);
                startActivity(intent);
            }
        });
    }
}
