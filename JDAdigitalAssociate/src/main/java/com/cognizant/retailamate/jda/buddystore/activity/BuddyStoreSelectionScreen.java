package com.cognizant.retailamate.jda.buddystore.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.cognizant.retailamate.jda.R;
import com.cognizant.retailamate.jda.buddystore.adapter.StoreAdapter;
import com.cognizant.retailamate.jda.buddystore.interfaces.ClearInterface;
import com.cognizant.retailamate.jda.buddystore.interfaces.StoreInterface;
import com.cognizant.retailamate.jda.buddystore.model.Store;
import com.cognizant.retailamate.jda.utils.Constants;
import com.google.gson.Gson;

import java.util.ArrayList;

public class BuddyStoreSelectionScreen extends AppCompatActivity implements StoreInterface, ClearInterface {

    private static final int RESULT_CLOSE_ALL = 0;

    private Gson gson;
    public static Store store;
    private ArrayList<Store.StoresBean> mStoresList;
    private RecyclerView mRecyclerView;

    private StoreAdapter mAdapter;
    private Context mContext;
    TextView storeName, productName, productIdtv, productCategory;
    int productRequired;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.buddy_stores_screen);


        imageView = (ImageView) findViewById(R.id.image_view);
        storeName = (TextView) findViewById(R.id.store_name);
        productName = (TextView) findViewById(R.id.product_name);
        productIdtv = (TextView) findViewById(R.id.product_id);
        productCategory = (TextView) findViewById(R.id.product_category);
        productRequired = getIntent().getIntExtra("quantity", 0);

        mContext = getApplicationContext();

        if (Constants.productId == 157760929)
            imageView.setImageResource(R.drawable.ic_157760929);
        else if (Constants.productId == 156859972)
            imageView.setImageResource(R.drawable.ic_156859972);
        else
            imageView.setImageResource(R.drawable.no_image);

        gson = new Gson();

        mStoresList = new ArrayList<>();

        String jsonString = "storeresponse_" + Constants.productId;

        store = gson.fromJson(Constants.loadJSONFromAsset(jsonString, mContext), Store.class);
        storeName.setText("");
        productName.setText(store.getName());
        productIdtv.setText(store.getID());
        productCategory.setText(store.getCategory());
        for (int i = 0; i < store.getStores().size(); i++) {
            mStoresList.add(store.getStores().get(i));
        }

        mRecyclerView = (RecyclerView) findViewById(R.id.store_info_recycler_view);

        mAdapter = new StoreAdapter(mStoresList, mContext, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (resultCode) {
            case RESULT_CLOSE_ALL:
                setResult(RESULT_CLOSE_ALL);
                finish();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void startActivity(int position) {
        Intent intent = new Intent(mContext, BuddyStoreDetailScreen.class);
        intent.putExtra("store", position);
        startActivityForResult(intent, RESULT_CLOSE_ALL);
    }

    @Override
    public void clearActivity() {
        finish();
    }
}
