package com.cognizant.retailamate.jda.orderdelivery;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.retailamate.jda.R;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Qr extends AppCompatActivity {

    ImageView qr;
    TextView order_id_qr, person_name, delivery_date, customer_id;
    Button order_delivered;
    String notification = "Invalid QR Code";
    String today, fed_content;         //today is to get the date and fed_content is to get the result from scannerActivity

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr);
        qr = (ImageView) findViewById(R.id.qr);
        order_id_qr = (TextView) findViewById(R.id.order_id_qr);
        person_name = (TextView) findViewById(R.id.person_name);
        delivery_date = (TextView) findViewById(R.id.delivery_date);
        order_delivered = (Button) findViewById(R.id.order_delivered);
        customer_id = (TextView) findViewById(R.id.customer_id);
        Intent intent = getIntent();
        fed_content = intent.getExtras().getString("Fed_String");
        putintoposition(fed_content);        //this method will place all the details of qrcode at their respective places in activity
        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {
            BitMatrix bitMatrix = multiFormatWriter.encode(fed_content, BarcodeFormat.QR_CODE, 550, 550);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
            qr.setImageBitmap(bitmap);

        } catch (WriterException e) {
            e.printStackTrace();
        }
        today = datefinder();                   //using this method we are getting the current date
        delivery_date.setText(today);
        order_delivered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Qr.this, "Order Delivered", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private String datefinder() {
        Date today = new Date();
        SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy");
        String date = DATE_FORMAT.format(today);
        return date;
    }

    void putintoposition(String content) {
        String delimiter = ":";
        String[] info = null;
        try {
            info = content.split(delimiter);
            order_id_qr.setText(info[0]);
            person_name.setText(info[2]);
            customer_id.setText(info[1]);

        } catch (ArrayIndexOutOfBoundsException e) {

            Toast.makeText(getApplicationContext(), notification, Toast.LENGTH_LONG).show();
            finish();
        }
    }
}
