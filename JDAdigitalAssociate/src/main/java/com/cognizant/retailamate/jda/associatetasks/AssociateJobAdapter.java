package com.cognizant.retailamate.jda.associatetasks;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cognizant.retailamate.jda.R;
import com.cognizant.retailamate.jda.associate.AssociateProfileActivity;
import com.google.gson.Gson;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * Created by 452781 on 11/11/2016.
 */

public class AssociateJobAdapter extends RecyclerView.Adapter<AssociateJobAdapter.myViewHolder> {

    private final LayoutInflater inflater;

    private static Context cxt;

    //    List<AssociateJobModelold.ResourceBean> dispdata= Collections.emptyList();
    List<AssociateOrderModel> dispdata;

    public AssociateJobAdapter(Context context, List<AssociateOrderModel> dispdata) {
        inflater = LayoutInflater.from(context);
        this.dispdata = dispdata;
        this.cxt = context;
    }

    @Override
    public myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.associate_job_list_layout, parent, false);
        myViewHolder holder = new myViewHolder(view);
        return holder;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(myViewHolder holder, final int position) {
        final AssociateOrderModel current = dispdata.get(position);

        try {
            if (current.getName().equals("Order Picking")) {
                holder.jobName.setText(current.getName());
                holder.jobField1.setText(current.getLocation());
//                holder.job_list_image.setBackgroundTintList(ContextCompat.getColorStateList(cxt, R.color.black));
                holder.jobField2Count.setText(String.valueOf(current.getQuantity()));
            } else if (current.getName().equals("Product Refill")) {
                holder.jobName.setText(current.getName());
                holder.jobField1.setText(current.getLocation());
                holder.jobField2.setText(current.getType());
                holder.jobField2Count.setVisibility(View.GONE);
                holder.associatetask_currenttaskview.setVisibility(View.INVISIBLE);
                holder.job_list_image.setImageResource(R.drawable.restore_shelf_inact);
                holder.linearLayout.setBackgroundResource(R.color.lightGrey2);
            } else {
                holder.jobName.setText(current.getName());
                holder.jobField1.setText(current.getLocation());
                holder.jobField2.setVisibility(View.GONE);
                holder.jobField2Count.setVisibility(View.GONE);
                holder.associatetask_currenttaskview.setVisibility(View.INVISIBLE);
                holder.job_list_image.setImageResource(R.drawable.empty_garbage_inact);
                holder.separater.setVisibility(View.GONE);
                holder.linearLayout.setBackgroundResource(R.color.lightGrey2);
            }

            holder.linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (current.getDatafromGson() == null || AssociateProfileActivity.fromManagerClass) {
                    } else {
                        Intent intent = new Intent(cxt, OrderDetails.class);
                        Gson gson = new Gson();
                        intent.putExtra("jobList", gson.toJson(current.getDatafromGson()));
//                intent.putExtra("jobListModel", (Serializable) current.getDatafromGson());
                        intent.putExtra("orderID", current.getLocation());
                        intent.putExtra("subtasksCount", String.valueOf(current.getQuantity()));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        cxt.startActivity(intent);
                    }
                }
            });
        } catch (NullPointerException | IndexOutOfBoundsException e) {

        }
    /*    if (current.getPriority()==1){
            holder.linearLayout.setBackgroundColor(Color.RED);

        }*/
    }

    @Override
    public int getItemCount() {
        return dispdata.size();
    }

    class myViewHolder extends RecyclerView.ViewHolder {
        TextView jobName, jobField1, jobField2, jobField2Count;
        LinearLayout linearLayout;
        View separater;
        ImageView job_list_image;
        RelativeLayout associatetask_currenttaskview;


        public myViewHolder(View itemView) {
            super(itemView);
            jobName = (TextView) itemView.findViewById(R.id.job_title);
            jobField1 = (TextView) itemView.findViewById(R.id.job_field1);
            jobField2 = (TextView) itemView.findViewById(R.id.job_field2);
            jobField2Count = (TextView) itemView.findViewById(R.id.job_count);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.layout);
            associatetask_currenttaskview = (RelativeLayout) itemView.findViewById(R.id.associatetask_currenttaskview);
            job_list_image = (ImageView) itemView.findViewById(R.id.job_list_image);
            separater = itemView.findViewById(R.id.separater);

        }
    }

}
