package com.cognizant.retailamate.jda.associate;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.cognizant.retailamate.jda.Model.AssociatePersonalDetailsModel;
import com.cognizant.retailamate.jda.Network.VolleyHelper;
import com.cognizant.retailamate.jda.R;
import com.cognizant.retailamate.jda.Service.AssociateSignalRService;
import com.cognizant.retailamate.jda.Service.BeaconService;
import com.cognizant.retailamate.jda.activity.SettingsActivity;
import com.cognizant.retailamate.jda.associatetasks.AssociateJobListActivity;
import com.cognizant.retailamate.jda.chatbot.UserAssociateChat;
import com.cognizant.retailamate.jda.orderdelivery.ZBarScannerActivity;
import com.cognizant.retailamate.jda.utils.AccountState;
import com.cognizant.retailamate.jda.utils.Constants;
import com.cognizant.retailamate.jda.utils.GlobalClass;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;

import microsoft.aspnet.signalr.client.Platform;
import microsoft.aspnet.signalr.client.http.android.AndroidPlatformComponent;

/**
 * Created by Bharath on 11/27/2016.
 */

public class AssociateHomeActivity extends AppCompatActivity {
    private static final String TAG = AssociateHomeActivity.class.getSimpleName();
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;
    ProgressDialog progress;
    private static final int REQUEST_CAMERA = 297;


    private final Context mContext = this;
    private boolean associateBound = false;
    private AssociateSignalRService associateService;
    View hView;
    TextView navAssociateName;
    TextView navAssociateJob;
    Gson gson;
    //    ImageView imageView;
    TextView name;

    ImageView nav_header_pic;

    LinearLayout customerInbound, productDelivery, customerFeedback;


    boolean isOpen = false;


    FloatingActionButton fab_p1us, fab_one, fab_two;
    Animation FabOpen, FabClose, FabClockwise, FabAntiClockwise;

    Button rotaupdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.associate_home);
//        imageView = (ImageView) findViewById(R.id.group_associate);
//        imageView.setImageResource(R.drawable.img1rewards);

//        GlobalClass.associateName=String.valueOf("Thomas Finey");

        customerInbound = (LinearLayout) findViewById(R.id.option_1);
        productDelivery = (LinearLayout) findViewById(R.id.option_2);
        customerFeedback = (LinearLayout) findViewById(R.id.option_3);

        customerInbound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent customer_intent = new Intent(AssociateHomeActivity.this, CustomerInBound.class);
                startActivity(customer_intent);
            }
        });

        productDelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent product_intent = new Intent(AssociateHomeActivity.this, ProductDelivery.class);
                startActivity(product_intent);
            }
        });

        customerFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent feedback_intent = new Intent(AssociateHomeActivity.this, CustomerFeedback.class);
                startActivity(feedback_intent);
            }
        });

        /*
        New FAB
         */
        fab_p1us = (FloatingActionButton) findViewById(R.id.fab_plus);
        fab_one = (FloatingActionButton) findViewById(R.id.fab_one);
        fab_two = (FloatingActionButton) findViewById(R.id.fab_two);
//        fab_three = (FloatingActionButton) findViewById(R.id.fab_three);
        FabOpen = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_open);
        FabClose = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_close);
        FabClockwise = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_clockwise);
        FabAntiClockwise = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_anticlockwise);

        rotaupdate = (Button) findViewById(R.id.rotaupdate);
//
//        fab_one.setImageResource(R.drawable.user_icon);
//        fab_two.setImageResource(R.drawable.in_store_offers_icon);
//        fab_three.setVisibility(View.INVISIBLE);

        fab_p1us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (isOpen) {
                    fab_one.startAnimation(FabClose);
                    fab_two.startAnimation(FabClose);
//                    fab_three.startAnimation(FabClose);
                    fab_p1us.startAnimation(FabAntiClockwise);
                    fab_one.setClickable(false);
                    fab_two.setClickable(false);
//                    fab_three.setClickable(false);
                    isOpen = false;
                } else {
                    fab_one.startAnimation(FabOpen);
                    fab_two.startAnimation(FabOpen);
//                    fab_three.startAnimation(FabOpen);
                    fab_p1us.startAnimation(FabClockwise);
                    fab_one.setClickable(true);
                    fab_two.setClickable(true);
//                    fab_three.setClickable(true);
                    isOpen = true;
                }


            }

        });


//        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Click action
                Intent intent = new Intent(AssociateHomeActivity.this, UserAssociateChat.class);

                startActivity(intent);
            }
        });

//        fabOffers = (FloatingActionButton) findViewById(R.id.fabOffers);
        fab_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                // Click action
//                Intent intent = new Intent(CatalogActivity.this, StoreOffers.class);
//                startActivity(intent);
            }
        });

        /*

        NEW FAB END
         */


        name = (TextView) findViewById(R.id.name);

        name.setText(String.valueOf(GlobalClass.associateName));
        Log.d("@@##", "BeaconService going to start soon ");
        getBaseContext().startService(
                new Intent(getBaseContext(), BeaconService.class));


        final float growTo = 1.0f;
        final long duration = 700;

        ScaleAnimation grow = new ScaleAnimation(5.0f, growTo, 5.0f, growTo,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        grow.setDuration(duration / 2);
        ScaleAnimation shrink = new ScaleAnimation(growTo, 1, growTo, 1,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        shrink.setDuration(duration / 2);
        shrink.setStartOffset(duration / 2);
        AnimationSet growAndShrink = new AnimationSet(true);
        growAndShrink.setInterpolator(new LinearInterpolator());
        growAndShrink.addAnimation(grow);
//        imageView.startAnimation(growAndShrink);

        gson = new Gson();
        Log.e(TAG, String.valueOf(GlobalClass.associateId));

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initNavigationDrawer();

        if (true) {


            AssociatePersonalDetailsModel[] associatePersonalDetailsModel = gson.fromJson(loadAssociateDetailsResponse(), AssociatePersonalDetailsModel[].class);

            GlobalClass.associateDesignation = associatePersonalDetailsModel[0].getJobName();
            GlobalClass.associateStartTime = associatePersonalDetailsModel[0].getStartTime();
            GlobalClass.associateEndTime = associatePersonalDetailsModel[0].getEndTime();

            navAssociateName.setText(String.valueOf(GlobalClass.associateName));
            navAssociateJob.setText(String.valueOf(GlobalClass.associateId));

            Intent associateIntent = new Intent();
            associateIntent.setClass(mContext, AssociateSignalRService.class);
            bindService(associateIntent, associateConnection, Context.BIND_AUTO_CREATE);

        } else {
            progress = ProgressDialog.show(AssociateHomeActivity.this, "Connecting",
                    "Please wait...", true);
            getAssociatePersonalDetails();
        }


        Platform.loadPlatformComponent(new AndroidPlatformComponent());


        nav_header_pic.setImageResource(getImageofAssociate());

        rotaupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callReportGenerationApi();
            }
        });
    }

    @Override
    public void onBackPressed() {
        this.finish();
    }

    public void getAssociatePersonalDetails() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.getAssociateDetailsURL + GlobalClass.associateId,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        progress.dismiss();

//                        AssociatePersonalDetailsModel associatePersonalDetailsModel = gson.fromJson(s, AssociatePersonalDetailsModel.class);
//
//                        GlobalClass.associateDesignation = associatePersonalDetailsModel.getJobName();
//                        GlobalClass.associateStartTime = associatePersonalDetailsModel.getStartTime();
//                        GlobalClass.associateEndTime = associatePersonalDetailsModel.getEndTime();

                        try {
                            AssociatePersonalDetailsModel[] associatePersonalDetailsModel = gson.fromJson(s, AssociatePersonalDetailsModel[].class);

                            GlobalClass.associateDesignation = associatePersonalDetailsModel[0].getJobName();
                            GlobalClass.associateStartTime = associatePersonalDetailsModel[0].getStartTime();
                            GlobalClass.associateEndTime = associatePersonalDetailsModel[0].getEndTime();

//                            if (associateConnection == null) {
                            Intent associateIntent = new Intent();
                            associateIntent.setClass(mContext, AssociateSignalRService.class);
                            bindService(associateIntent, associateConnection, Context.BIND_AUTO_CREATE);
//                            }
//                        AssociateScheduleModel associateScheduleModel = gson.fromJson(s, AssociateScheduleModel.class);
//
//                        GlobalClass.associateDesignation = associateScheduleModel.getAssociateSchedule().get(0).getJobName();
//                        GlobalClass.associateStartTime = associateScheduleModel.getAssociateSchedule().get(0).getStartTime();
//                        GlobalClass.associateEndTime = associateScheduleModel.getAssociateSchedule().get(0).getEndTime();

                            navAssociateName.setText(String.valueOf(GlobalClass.associateName));
//                            navAssociateName.setText("Thomas Finey");
                            navAssociateJob.setText(String.valueOf(GlobalClass.associateId));
                        } catch (ArrayIndexOutOfBoundsException | NullPointerException | IllegalArgumentException e) {
//                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(AssociateHomeActivity.this);
//                            alertDialog.setTitle("Shift Details")
//                                    .setMessage("You do not have any scheduled tasks at the moment.")
//                                    .setCancelable(false)
//                                    .setIconAttribute(android.R.attr.alertDialogIcon)
//                                    .setPositiveButton(
//                                            "OK",
//                                            new DialogInterface.OnClickListener() {
//                                                public void onClick(DialogInterface dialog, int id) {
//                                                    dialog.cancel();
//                                                    finish();
//                                                }
//                                            })
//                                    .show();
                        } catch (IllegalStateException x) {
                            Toast.makeText(AssociateHomeActivity.this, "Something went wrong. Please try again.", Toast.LENGTH_LONG).show();

                            finish();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progress.dismiss();
                        Toast.makeText(AssociateHomeActivity.this, "Login failed. Please try again.", Toast.LENGTH_LONG).show();
                        finish();
                        Log.e("@@##", "VolleyError = " + error);
                        Log.e("@@##", "VolleyError = " + error.getMessage());

                    }

                });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES * 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyHelper.getInstance(AssociateHomeActivity.this).addToRequestQueue(stringRequest);
    }

    public void initNavigationDrawer() {

        NavigationView navigationView = (NavigationView) findViewById(R.id.associate_navigation_view);

        hView = navigationView.getHeaderView(0);

        navAssociateName = (TextView) hView.findViewById(R.id.associate_name_nav);

        navAssociateJob = (TextView) hView.findViewById(R.id.associate_job_nav);

        nav_header_pic = (ImageView) hView.findViewById(R.id.nav_header_pic);


        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                int id = menuItem.getItemId();

                switch (id) {
                    case R.id.associate_my_tasks:
                        Intent intent = new Intent(AssociateHomeActivity.this, AssociateJobListActivity.class);
                        startActivity(intent);
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.associate_store_view:
                        Toast.makeText(getApplicationContext(), "You don't have permission to access this feature.", Toast.LENGTH_SHORT).show();
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.associate_my_profile:
                        Toast.makeText(getApplicationContext(), "My Profile", Toast.LENGTH_SHORT).show();
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.associate_settings:
                        Intent intent1 = new Intent(AssociateHomeActivity.this, SettingsActivity.class);
                        intent1.putExtra("userrole", "associate");
                        startActivity(intent1);
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.associate_logout:
//                        Toast.makeText(getApplicationContext(), "Logout", Toast.LENGTH_SHORT).show();
                        drawerLayout.closeDrawers();
                        finish();
                        break;
                }
                return true;
            }
        });

        drawerLayout = (DrawerLayout) findViewById(R.id.associate_drawer);

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerClosed(View v) {
                super.onDrawerClosed(v);
            }

            @Override
            public void onDrawerOpened(View v) {
                super.onDrawerOpened(v);
            }
        };
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    ServiceConnection associateConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            AssociateSignalRService.LocalBinder binder = (AssociateSignalRService.LocalBinder) service;
            associateService = binder.getService();
            associateBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
        }
    };

    @Override
    protected void onDestroy() {

        try {
            unbindService(associateConnection);
            getBaseContext().startService(
                    new Intent(getBaseContext(), AssociateSignalRService.class));

        } catch (IllegalArgumentException e) {

        }
        super.onDestroy();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.landingpagemenu, menu);
        // setMenuBackground();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle action bar actions click
        switch (item.getItemId()) {

            case R.id.qrscan:
                if (GlobalClass.checkPermission(this, Manifest.permission.CAMERA)) {
                    Intent intent = new Intent(getApplicationContext(),
                            ZBarScannerActivity.class);
                    startActivity(intent);
                } else {
                    requestPermission(this, REQUEST_CAMERA, Manifest.permission.CAMERA);
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void requestPermission(Activity activity, int permissionRequestCode, String permission) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
            Toast.makeText(activity, "Please allow permission in App Settings for additional functionality.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(activity, new String[]{permission}, permissionRequestCode);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {

            case REQUEST_CAMERA:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    //TODO
                }
                break;
        }
    }

    private int getImageofAssociate() {

        if (AccountState.getPrefUserId().equals("E100997294")) {
            return R.drawable.callistar;
        } else if (AccountState.getPrefUserId().equals("1000247")) {
            return R.drawable.bush;
        } else {
            return R.drawable.blue_profile;
        }

    }


    private void callReportGenerationApi() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.sendReport,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        Log.e("@@##", "Response = " + s);
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.e("@@##", "VolleyError = " + error);
                        Log.e("@@##", "VolleyError = " + error.getMessage());

                    }

                });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES * 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyHelper.getInstance(AssociateHomeActivity.this).addToRequestQueue(stringRequest);
    }

    public String loadAssociateDetailsResponse() {
        String json = null;
        try {
            InputStream is = getAssets().open("AssociateDetailsResponse.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}