package com.cognizant.retailamate.jda.associatetasks;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.cognizant.retailamate.jda.R;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

/**
 * Created by 543898 on 12/5/2016.
 */
public class DetailsFragment extends AppCompatActivity {

    AssociateJobModel.ResourceBean productDetail;
    TextView product_name, product_id, availability, quantity, description, location;
    ImageView product_image;
    Gson gson;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_details_pop_up);

        product_name = (TextView) findViewById(R.id.product_name);
        product_id = (TextView) findViewById(R.id.product_id);
        location = (TextView) findViewById(R.id.location);
        availability = (TextView) findViewById(R.id.availability);
        quantity = (TextView) findViewById(R.id.quantity);
        description = (TextView) findViewById(R.id.description);
        product_image = (ImageView) findViewById(R.id.product_image);


        gson = new Gson();
        Log.e("####", getIntent().getStringExtra("productDetails"));
        productDetail = gson.fromJson(getIntent().getStringExtra("productDetails"), AssociateJobModel.ResourceBean.class);
//        jobList= (AssociateJobModelold.ResourceBean) getIntent().getSerializableExtra("jobList");

        product_name.setText(productDetail.getItemName());
        location.setText(productDetail.getLocation());
        quantity.setText(String.valueOf(productDetail.getQuantity()));
        description.setText(productDetail.getItemName());
        product_id.setText(productDetail.getItemNum());

//        System.out.println("@@## IMAGE URL " + productDetail.getItemImage());
//        String encodedDataString = productDetail.getItemImage();
//
//        encodedDataString = encodedDataString.replace("data:image/jpeg;base64,", "");
//
//        byte[] imageAsBytes = Base64.decode(encodedDataString.getBytes(), 0);
//        product_image.setImageBitmap(BitmapFactory.decodeByteArray(
//                imageAsBytes, 0, imageAsBytes.length));
//        product_image.setImageResource(R.drawable.no_image);

        Picasso.with(this).load("https://Nikedevret.cloudax.dynamics.com/MediaServer/Products/" + productDetail.getItemNum() + "_000_001.png")
                .placeholder(R.drawable.no_image)
                .into(product_image);
        availability.setText(productDetail.getOrderNum());
    }

    public void closeFragment(View view) {
        finish();


    }
}