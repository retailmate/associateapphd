package com.cognizant.retailamate.jda.associate;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.cognizant.retailamate.jda.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class CustomerLocation extends AppCompatActivity implements OnMapReadyCallback {

    Toolbar toolbar;

    MapFragment mapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.customer_inbound_location);

//        toolbar = (Toolbar) findViewById(R.id.toolbar_1);
//        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        googleMap.getUiSettings().setZoomControlsEnabled(false);

        // Enable / Disable my location button
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);

        // Enable / Disable Compass icon
        googleMap.getUiSettings().setCompassEnabled(true);

        // Enable / Disable Rotate gesture
        googleMap.getUiSettings().setRotateGesturesEnabled(true);

        // Enable / Disable zooming functionality
        googleMap.getUiSettings().setZoomGesturesEnabled(true);


        LatLng landmarkstore = new LatLng(25.119008, 55.202874);

//        map.moveCamera(CameraUpdateFactory.newLatLng(landmarkstore));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(landmarkstore, (float) 14.0));

        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(25.120417, 55.197939))
                .title("Amer")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.blue_pple))
        );

        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(25.119008, 55.202874))
                .title("Homecentre Store"));

//        map.getMaxZoomLevel();

    }
}
