package com.cognizant.retailamate.jda.associatetasks;

import java.util.List;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by 452781 on 11/25/2016.
 */
public class AssociateJobModel {


//    private List<ResourceBean> resource;
//
//    public List<ResourceBean> getResource() {
//        return resource;
//    }
//
//    public void setResource(List<ResourceBean> resource) {
//        this.resource = resource;
//    }

    @SerializedName("resource")
    @Expose
    private List<ResourceBean> resource = null;

    public List<ResourceBean> getResource() {
        return resource;
    }

    public void setResource(List<ResourceBean> resource) {
        this.resource = resource;
    }


    public static class ResourceBean {

        //        private String type;
//        private int quantity;
//        private String itemName;
//        private String location;
        private boolean isselected;
        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("priority")
        @Expose
        private String priority;
        @SerializedName("quantity")
        @Expose
        private Integer quantity;
        @SerializedName("location")
        @Expose
        private String location;
        @SerializedName("orderNum")
        @Expose
        private String orderNum;
        @SerializedName("customer")
        @Expose
        private String customer;
        @SerializedName("assocID")
        @Expose
        private Integer assocID;
        @SerializedName("assignee")
        @Expose
        private String assignee;
        @SerializedName("itemNum")
        @Expose
        private String itemNum;
        @SerializedName("itemName")
        @Expose
        private String itemName;
//        @SerializedName("itemImage")
//        @Expose
//        private String itemImage;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getPriority() {
            return priority;
        }

        public void setPriority(String priority) {
            this.priority = priority;
        }

        public Integer getQuantity() {
            return quantity;
        }

        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getOrderNum() {
            return orderNum;
        }

        public void setOrderNum(String orderNum) {
            this.orderNum = orderNum;
        }

        public String getCustomer() {
            return customer;
        }

        public void setCustomer(String customer) {
            this.customer = customer;
        }

        public Integer getAssocID() {
            return assocID;
        }

        public void setAssocID(Integer assocID) {
            this.assocID = assocID;
        }

        public String getAssignee() {
            return assignee;
        }

        public void setAssignee(String assignee) {
            this.assignee = assignee;
        }

        public String getItemNum() {
            return itemNum;
        }

        public void setItemNum(String itemNum) {
            this.itemNum = itemNum;
        }

        public String getItemName() {
            return itemName;
        }

        public void setItemName(String itemName) {
            this.itemName = itemName;
        }

//        public String getItemImage() {
//            return itemImage;
//        }
//
//        public void setItemImage(String itemImage) {
//            this.itemImage = itemImage;
//        }

//        public String getType() {
//            return type;
//        }
//
//        public void setType(String type) {
//            this.type = type;
//        }
//
//        public int getQuantity() {
//            return quantity;
//        }
//
//        public void setQuantity(int quantity) {
//            this.quantity = quantity;
//        }
//
//        public String getItemName() {
//            return itemName;
//        }
//
//        public void setItemName(String itemName) {
//            this.itemName = itemName;
//        }
//
//        public String getLocation() {
//            return location;
//        }
//
//        public void setLocation(String location) {
//            this.location = location;
//        }

        public boolean isselected() {
            return isselected;
        }

        public void setIsselected(boolean isselected) {
            this.isselected = isselected;
        }
    }
}
