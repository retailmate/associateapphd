package com.cognizant.retailamate.jda.buddystore.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;


import com.cognizant.retailamate.jda.R;
import com.cognizant.retailamate.jda.buddystore.interfaces.ProductInterface;
import com.cognizant.retailamate.jda.buddystore.model.Product;

import java.util.List;

/**
 * Created by Bharath on 23/03/17.
 */

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.MyViewHolder> {

    private List<Product.VariantsBean> productList;

    private Context mContext;

    RadioButton lastSelected;

    ProductInterface productInterface;

    public static int lastCheckedPos;

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView size, color, price, soh_shelf, soh_back_room;

        public RadioButton radioButton;


        MyViewHolder(View view) {
            super(view);

            radioButton = (RadioButton) view.findViewById(R.id.radio_button);
            size = (TextView) view.findViewById(R.id.product_size);
            price = (TextView) view.findViewById(R.id.product_price);
            color = (TextView) view.findViewById(R.id.product_color);
            soh_shelf = (TextView) view.findViewById(R.id.product_SOH_shelf);
            soh_back_room = (TextView) view.findViewById(R.id.product_SOH_back_room);


        }
    }


    public ProductAdapter(List<Product.VariantsBean> productList, Context mContext, ProductInterface productInterface) {
        this.productList = productList;
        this.mContext = mContext;
        this.productInterface = productInterface;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_detail_item, parent, false);
        lastCheckedPos = -1;
        lastSelected = null;
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Product.VariantsBean product = productList.get(position);
        holder.size.setText(product.getSize());
        holder.price.setText(product.getPrice());
        holder.color.setText(product.getColor());

        holder.soh_shelf.setText(product.getSOH().getShelf());
        holder.soh_back_room.setText(product.getSOH().getBackRoom());

        if (Integer.parseInt(productList.get(position).getSOH().getShelf()) == 0 && Integer.parseInt(productList.get(position).getSOH().getBackRoom()) == 0) {
            holder.soh_shelf.setTextColor(Color.RED);
            holder.soh_back_room.setTextColor(Color.RED);
        }

        holder.radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (lastCheckedPos == -1) {
                    productInterface.enableButton();
                    lastSelected = (RadioButton) v;
                    lastCheckedPos = position;
                } else {

                    if (v.equals(lastSelected)) {
                        holder.radioButton.setChecked(true);
                    } else {
                        productInterface.setIfDisabled();
                        lastSelected.setChecked(false);
                        lastSelected = (RadioButton) v;
                        lastCheckedPos = position;

                    }
                }

            }
        });


    }

    @Override
    public int getItemCount() {
        return productList.size();
    }
}